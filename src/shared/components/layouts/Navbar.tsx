import * as React from 'react';
import { useState, useEffect } from 'react';
import { Colors } from '@/style/colors';
import {
  AppBar,
  Stack,
  Toolbar,
  Typography,
} from '@mui/material';
import Image from 'next/image';
import './navbar.scss';
import { useRouter } from 'next/navigation';
import { usePathname } from 'next/navigation'

const menus = [
  { 'shop': ['pans', 'tools', 'kitchens'] },
  { 'recipies': ['categories', 'collections', 'resources'] },
  { 'learn': ['articles', 'videos', 'resources'] },
  { 'about': ['about us', 'contact'] },
  { 'blog': ['blog'] }
];

function ResponsiveAppBar() {
  const pathname = usePathname();

  const router = useRouter();

  const [selectedMainMenu, setSelectedMainMenu] = useState<string>('');
  const [currentSubMenu, setCurrentSubMenu] = useState<string[]>([]);
  const [selectedSubMenu, setSelectedSubMenu] = useState<any>([]);

  useEffect(() => {
    const subMenu = pathname.split('/').filter(i => i !== '')[0];
    const getSelectedMainMenu = menus
      .find(i => {
        return Object.values(i)[0].includes(subMenu)
      });

    if (getSelectedMainMenu) {
      const currentMenu: string = Object.keys(getSelectedMainMenu)[0];
      setSelectedMainMenu(currentMenu);
      const getSubMenu: any = menus.find(i => Object.keys(i)[0] === currentMenu);
      getSubMenu && getSubMenu[currentMenu] && setCurrentSubMenu(getSubMenu[currentMenu]);
      setSelectedSubMenu(subMenu);
    } else {
      setSelectedMainMenu('shop');
      const getSubMenu: any = menus.find(i => Object.keys(i)[0] === 'shop');
      getSubMenu && getSubMenu.shop && setCurrentSubMenu(getSubMenu.shop);
      setSelectedSubMenu('pans');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  return (
    <AppBar position="static" sx={{ boxShadow: 'none', }} >
      <Image src='/shop.png' width={100} height={100} alt="Cover Image" className="bg-img" />
      <Stack>
        <Toolbar disableGutters sx={{ backgroundColor: Colors.backgroundColor, height: 80 }}>
          <Stack direction='row' spacing={6} sx={{ marginLeft: 30 }}>
            {menus.map((menu, index) => {
              const item = Object.keys(menu)[0];
              return <Typography
                key={index}
                color={Colors.text}
                textTransform='uppercase'
                fontWeight={600}
                fontSize={18}
                sx={{
                  cursor: 'pointer',
                  borderBottom: selectedMainMenu === item ? `3px solid ${Colors.orange}` : ''
                }}
                onClick={() => {
                  setSelectedMainMenu(item);
                  setCurrentSubMenu(Object.values(menu)[0]);
                  setSelectedSubMenu('');
                }}
              >
                {item}
              </Typography>
            })}
          </Stack>
        </Toolbar>
        <Toolbar disableGutters sx={{ backgroundColor: Colors.subMenubg }}>
          <Stack direction='row' spacing={6} sx={{ marginLeft: 30 }}>
            {currentSubMenu.map((menu, index) => {
              return <Typography
                key={index}
                color={Colors.text}
                textTransform='uppercase'
                fontWeight={600}
                fontSize={16}
                sx={{
                  cursor: 'pointer',
                  borderBottom: selectedSubMenu === menu ? `3px solid ${Colors.orange}` : ''
                }}
                onClick={() => {
                  setSelectedSubMenu(menu);
                  router.push(`/${menu}`);
                }}
              >
                {menu}
              </Typography>
            })}
          </Stack>
        </Toolbar>
      </Stack >
    </AppBar >
  );
}
export default ResponsiveAppBar;
