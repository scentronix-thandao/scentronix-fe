'use client';

import { Colors } from "@/style/colors";
import { Add, Print } from "@mui/icons-material";
import { Alert, Box, Button, Snackbar, Typography } from "@mui/material";
import { useState } from "react";

interface Props {
  type: string
}

export default function CustomButton(props: Props) {
  const { type } = props;
  const [isClick, setIsClick] = useState<boolean>(false);

  return <Box>
    <Button
      variant="outlined"
      color="warning"
      startIcon={type === 'Print'
        ? <Print style={{ fontSize: 22, color: Colors.text }} />
        : <Add style={{ fontSize: 22, color: Colors.text }} />}
      style={{ border: '1.2px solid' }}
    >
      <Typography
        fontSize={13}
        fontWeight={700}
        p={1}
        color={Colors.text}
        onClick={() => setIsClick(true)}
      >
        {type}
      </Typography>
    </Button>
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isClick}
      onClose={() => setIsClick(false)}
      autoHideDuration={4000}
    >
      <Alert
        onClose={() => setIsClick(false)}
        severity="info"
        variant="filled"
        sx={{ width: '100%' }}
      >
        {type}
      </Alert>
    </Snackbar>
  </Box>
}