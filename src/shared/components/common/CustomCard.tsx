"use client"

import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography
} from '@mui/material';
import * as React from 'react';
import { useRouter } from 'next/navigation';

interface Props {
  page: string,
  bread: string
}

export default function CustomCard(props: Props) {
  const router = useRouter();

  const { page, bread } = props;

  const convertBreadName = bread.toLowerCase().replaceAll('-', '_').replaceAll(' ', '_');
  console.log(convertBreadName, 'convertBreadName');

  return (
    <Card sx={{ maxWidth: 345 }} onClick={() => {
      router.push(`/${page}/bread/quick_bread/${convertBreadName}`)
    }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="140"
          image={`/bread/${convertBreadName}.jpeg`}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="div">
            {bread}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
