import { Colors } from "@/style/colors";
import { Box, Grid, Stack, Typography } from "@mui/material";
import { NavigateNextRounded as NavigateNextRoundedIcon } from '@mui/icons-material';
import CustomCard from "../common/CustomCard";

interface Props {
  page: string
}

export default function Category(props: Props) {
  const { page } = props;

  const breads = [
    'White Sandwich Bread',
    'Whole-Wheat',
    'Whole-Grain',
    'Sprouted-Grain',
    'Pumpernickel',
    'Sourdough',
    'Focaccia'
  ];

  return <Grid container spacing={2}>
    <Grid item xs={4}>
      <Box sx={{
        height: 200,
        p: 4,
        mx: 4,
        backgroundColor: Colors.subMenubg
      }}>

        <Typography variant="h5"
          fontWeight={600}
          color={Colors.text}
          sx={{ borderBottom: `3px solid ${Colors.text}` }}
        >
          Bread
        </Typography>
        <Stack direction='row' alignItems='center' spacing={2}
          sx={{ cursor: 'pointer' }}
          pt={4}
          pl={4}
        >
          <Typography variant="h6"
            fontWeight={600}
            color={Colors.text}
          >
            Quick Bread
          </Typography>
          <NavigateNextRoundedIcon />
        </Stack>
      </Box>
    </Grid>
    <Grid item xs={8}>
      <Typography variant="h4" pb={4}>Quick Bread</Typography>
      <Grid container>
        {breads.map((bread, index) => {
          return <Grid item xs={4} key={index} p={4} >
            <CustomCard page={page} bread={bread} />
          </Grid>
        })}
      </Grid>
    </Grid>
  </Grid>
}