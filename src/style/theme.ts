'use client';

import { Roboto } from 'next/font/google';
import { makeStyles } from '@mui/styles';

const roboto = Roboto({
  weight: ['300', '400', '500', '700'],
  subsets: ['latin'],
  display: 'swap',
});

const theme = makeStyles({
  typography: {
    fontFamily: roboto.style.fontFamily,
  },
  root: {
    backgroundColor: '#fff'
  }
});

export default theme;
