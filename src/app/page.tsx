import { Box, Stack, Typography } from "@mui/material";
import { NavigateNextRounded as NavigateNextRoundedIcon } from '@mui/icons-material';
import { Colors } from "@/style/colors";

export default function Home() {
  return (
    <Box p={8}>
      <Typography variant="h6" color={Colors.text} pb={4}>Home page comming soon!</Typography>
      <Stack direction='row' spacing={1}>
        <Typography color={Colors.text}>Please choose</Typography>
        <Typography color={Colors.orange}>Recipes</Typography>
        <NavigateNextRoundedIcon color='warning' />
        <Typography color={Colors.orange}>Categories</Typography>
        <Typography color={Colors.text} >to test app</Typography>
      </Stack>
    </Box>
  );
}
