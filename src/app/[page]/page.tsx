import Category from "@/shared/components/categories/Category";
import { Colors } from "@/style/colors";
import { Box, Stack, Typography } from "@mui/material";

interface Params {
  page: string
}

export default function Page({ params }: { params: Params }) {
  return <Box sx={{ px: 8, py: 4 }}>
    {params.page === 'categories' ?
      <Category page={params.page} />
      :
      <Stack direction='row' alignItems='center' spacing={0.8}>
        <Typography variant="h5" color={Colors.text}>This is</Typography>
        <Typography variant="h5" fontWeight={600} color={Colors.orange}>{params.page}</Typography>
        <Typography variant="h5" color={Colors.text}>page</Typography>
      </Stack>}
  </Box>
}