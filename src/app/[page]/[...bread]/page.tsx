import {
  Box,
  Container,
  Divider,
  Grid,
  Stack,
  Typography
} from "@mui/material";
import {
  NavigateNextRounded as NavigateNextRoundedIcon,
  BlurCircularRounded as BlurCircularRoundedIcon,
} from '@mui/icons-material';
import _ from 'lodash';
import { Colors } from "@/style/colors";
import Image from "next/image";
import { QueryBuilder as QueryBuilderIcon } from '@mui/icons-material';
import CustomButton from "@/shared/components/common/CustomButton";

interface Params {
  page: string,
  bread: string[]
}

async function getArtist() {
  const id = _.random(100);

  const fetchData = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
  return fetchData.json()
}

export default async function BreadPage({ params }: { params: Params }) {
  const getPost = getArtist();
  const [post] = await Promise.all([getPost])

  const title = _.startCase(
    params.bread
      .slice(params.bread.length - 1, params.bread.length)
      .toString()
      .replaceAll('_', ' ')
  );

  return <Container >
    <Box sx={{ py: 8 }}>
      <Grid container sx={{ pt: 6 }}>
        <Grid item xs={7}>
          <Stack direction='row' spacing={0.4}>
            <Typography fontSize={15} fontWeight={600} color={Colors.text}>
              {params.page.toUpperCase().replaceAll('_', ' ')}
            </Typography>
            <NavigateNextRoundedIcon color="warning" />

            {params.bread.slice(0, -1).map((item, index) => {
              return <Stack key={index} direction='row'>
                <Typography fontSize={15} fontWeight={600} color={Colors.text}>
                  {item.toUpperCase().replaceAll('_', ' ')}
                </Typography>
                {index < params.bread.length - 1 && <NavigateNextRoundedIcon color="warning" />}
              </Stack>
            })}
          </Stack>
          <Typography pt={4} variant="h3" color={Colors.text}>
            {title}
          </Typography>
          <Stack>
            <Typography color={Colors.orange} fontSize={18} fontStyle='italic' pt={8}>
              This is dummy data gets from  jsonplaceholder API:
            </Typography> <Typography color={Colors.text} pt={1}>
              {post.body}
            </Typography>
          </Stack>
          <Stack
            direction='row'
            justifyContent='space-between'
            pt={6} mr={4}
            alignItems='center'
          >
            <Stack direction='row' alignItems='center' spacing={2}>
              <QueryBuilderIcon
                sx={{ height: 50, width: 50, color: Colors.text }}
              />
              <Box>
                <Typography
                  fontSize={12}
                  fontWeight={600}
                  color={Colors.text}
                >
                  PREP
                </Typography>
                <Typography
                  fontWeight={600}
                  color={Colors.text}
                >
                  10 mins
                </Typography>
              </Box>
            </Stack>
            <Stack>
              <Typography
                fontSize={12}
                fontWeight={600}
                color={Colors.text}
              >
                BAKE
              </Typography>
              <Typography
                fontWeight={600}
                color={Colors.text}
              >
                1 hr to 1 hr 15 mins
              </Typography>
            </Stack>
            <Stack>
              <Typography
                fontSize={12}
                fontWeight={600}
                color={Colors.text}
              >
                TOTAL
              </Typography>
              <Typography
                fontWeight={600}
                color={Colors.text}
              >
                1 hr 10 mins
              </Typography>
            </Stack>
          </Stack>
          <Divider sx={{ pt: 2 }} />
          <Stack
            direction='row'
            justifyContent='space-between'
            pt={6} mr={4}
            alignItems='center'
          >
            <Stack
              direction='row'
              alignItems='center'
              spacing={2}
            >
              <BlurCircularRoundedIcon
                sx={{ height: 50, width: 50, color: Colors.text }}
              />
              <Box>
                <Typography
                  fontSize={12}
                  fontWeight={600}
                  color={Colors.text}
                >
                  YEILD
                </Typography>
                <Typography
                  fontWeight={600}
                  color={Colors.text}
                  width={160}
                >
                  1 loaf, 12 generous servings
                </Typography>
              </Box>
            </Stack>
            <CustomButton type="Save Recipie" />
            <CustomButton type="Print" />
          </Stack>
        </Grid>
        <Grid item xs={5} pl={8}>
          <Image
            src={`/bread/${params.bread.pop()}.jpeg`}
            width={500}
            height={500}
            alt="Picture of the bread"
          />
        </Grid>
      </Grid>
    </Box >
  </Container >
}