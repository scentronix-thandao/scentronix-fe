<div align="center">
  <h2 align="center">Scentronix Frontend Assignment</h2>
  <p align="center">
    The Scentronix Frontend documentation
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details open>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#requirement-diagram">Requirement</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
## About The Project

### Requirement Diagram

![Product Name ScreenShot][requirement-screenshot]

Re-create this page using the following tools.

- Everything is done through git, so to share this project we would like you to send us a git repo on for example like Github/Gitlab etc.
- We would like you to use Next.js as a start for this assignment, and in our apps we use Material-UI as a base for our components.
- Create a few pages like this with different kind of text and link them together in the menu.
- Have a dynamic way of providing data into the components using the best NextJS practices.
- Images and icons you can use either random from the internet or other packages.

### Built With

This project uses NodeJS (Typescript) and format by Eslint and Prettier, then the testing used Jest.

- [![NextJS][Next.js]][Next-url]
- [![Typescript][Typescript]][Typescript-url]
- [![Eslint][Eslint]][Eslint-url]
- [![Prettier][Prettier]][Prettier-url]


<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Installation

Below are the steps how to clone and run project locally

1. Clone the repo
   ```sh
   git clone https://gitlab.com/scentronix-thandao/scentronix-fe.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
   or
   ```sh
   yarn
   ```
3. The scripts to run project
   ```json
    "dev": "next dev",
    "build": "next build",
    "start": "next start",
    "lint": "next lint"
   ```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

![Project output ScreenShot][output-screenshot]

<!-- USAGE EXAMPLES -->
## Usage

Use these scripts to run and test project

- Check lint code by Eslint and Prettier
```sh
yarn lint or npm run lint
```

- Run code with dev mode (dev environment)
```sh
yarn dev
```

- Build code
```sh
yarn build or npm run build
```

- Run code as production mode
```sh
yarn start or npm run start
```

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the source better. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

[Than Dao](daothan1211@gmail.com)

<!-- CONTACT -->
## Contact

Than Dao - daothan1211@gmail.com

Project Link: [https://gitlab.com/scentronix-thandao/scentronix-fe.git](https://gitlab.com/scentronix-thandao/scentronix-fe.git)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[requirement-screenshot]: public/website.jpeg
[output-screenshot]: public/output.png

[Next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[Next-url]: https://nextjs.org

[Typescript]: https://img.shields.io/badge/typescript-000000?style=for-the-badge&logo=typescript&logoColor=blue
[Typescript-url]: https://www.typescriptlang.org

[Eslint]: https://img.shields.io/badge/eslint-000000?style=for-the-badge&logo=eslint&logoColor=4b32c3
[Eslint-url]: https://eslint.org

[Prettier]: https://img.shields.io/badge/prettier-000000?style=for-the-badge&logo=prettier&logoColor=4b32c3
[Prettier-url]: https://prettier.io